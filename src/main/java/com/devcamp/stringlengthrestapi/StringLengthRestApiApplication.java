package com.devcamp.stringlengthrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StringLengthRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(StringLengthRestApiApplication.class, args);
	}

}
